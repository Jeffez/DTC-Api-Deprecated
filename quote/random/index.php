<?php
include('../../simple_html_dom.php');

//header('Content-type: application/json; charset=ISO-8859-1');

if(true){
    $content = getContent();

    $quoteId = getQuoteId($content);
    $content = getContentWithId($quoteId);
    $plusCount = getPlus($content, $quoteId) . "";
    $plusCount = substr($plusCount,4);
    $minusCount = getMinus($content, $quoteId) . "";
    $minusCount = substr($minusCount,4);
    $quote = getQuote($content, $quoteId);

    echo json_encode(array(
            'quoteId' => $quoteId,
            'quote' => $quote,
            'plus' => $plusCount ,
            'minus' => $minusCount
        ), JSON_PRETTY_PRINT);
}

function getQuoteId($content){
    $quoteId = $content->find('span[class=item-infos]')[rand(0,24)];
    return $quoteId->id;
}

function getContent(){
        $cl = file_get_contents('https://danstonchat.com/random.html');
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getContentWithId($quoteId){
        $cl = file_get_contents('https://danstonchat.com/' . $quoteId . '.html');
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getQuote($content, $quoteId){
    $comments = $content->find('p[class=item-content]')[0]->children(0);
    $commentsWithoutBr = preg_replace("/\<br (.*?)\>/","",$comments->innertext);
    $commentsId = preg_replace("/\<span class=\"decoration\"\>/","</span>",$commentsWithoutBr);
    $commentsIdSplit = preg_split("/\<\/span\>/",$commentsId);
    $answer = array();
    for($i=1;$i<count($commentsIdSplit);$i+=2){
        $answer[] = array('user' => $commentsIdSplit[$i],
                        'sentence' => $commentsIdSplit[$i+1]);
    }
    return $answer;
}

function getMinus($content, $quoteId){
    $div = $content->find('a[href=https://danstonchat.com/voteminus/' . $quoteId . '.html]');
    return $div[0]->plaintext;
}

function getPlus($content, $quoteId){
    $div = $content->find('a[href=https://danstonchat.com/voteplus/' . $quoteId . '.html]');
    return $div[0]->plaintext;
}
