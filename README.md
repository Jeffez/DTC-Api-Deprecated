# DTC API (Dans Ton Chat)

 ## DEPRECATED
 
 Suite à une mise à jour Graphique, cette API est maintenant inutilisable.


 ## /!\ A LIRE
 
``` 
Cette API est une API NON-OFFICIELLE. Elle a était créé avec l'autorisation de Remouk.
Si vous souhaitez utiliser cette API, vous DEVEZ demander à Remouk l'autorisation.
Dans le cas où vous utilisez cette API sans l'obtention de cette autorisation, vous ne faites que risquer la suppression de cette API, et le blocage de la methode utilisé

Pour contacter Remouk, merci d'envoyer un mail à "admin@danstonchat.com".
Je vous conseil d'y indiquer la raison pour laquelle vous souhaitez utiliser cette API, ainsi que l'utilisation estimé.

Cette API utilise également simple_html_dom (http://simplehtmldom.sourceforge.net/) pour parser les pages de DTC.
```

 ## Fonctionnement de l'API
 
Pour installer l'API sur votre site web, il vous suffit de copier les fichiers.

Une fois ceci fait, il vous suffit d'effectuer des requêtes GET, comme indiqué dans le Wiki. Un Json adapté vous sera alors renvoyé.
