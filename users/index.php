<?php
include('../simple_html_dom.php');

header('Content-type: application/json; charset=UTF-8');

if(isset($_GET['userId'])){

    $userId = $_GET['userId'];

    $content = getContent($userId);

    $pseudo = getPseudo($content, $userId) . "";
    $prenom = getPrenom($content, $userId) . "";
    $nom = getNom($content, $userId) . "";
    $age = getAge($content, $userId) . "";
    $ville = getVille($content, $userId) . "";
    $pays = getPays($content, $userId) . "";
    $avatarLink = getAvatarLink($content, $userId) . "";


    echo json_encode(array(
            'pseudo' => $pseudo ,
            'prenom' => $prenom ,
            'nom' => $nom,
            'age' => $age ,
            'ville' => $ville ,
            'pays' => $pays,
            'avatarLink' => $avatarLink
        ));
}else{
    echo json_encode(array(
            'pseudo' => '' ,
            'prenom' => '' ,
            'nom' => '',
            'age' => '' ,
            'ville' => '' ,
            'pays' => '',
            'avatarLink' => ''
        ));
}


function getContent($userId){
        $cl = file_get_contents('https://danstonchat.com/geek/' . $userId . '.html');
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getPseudo($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(0);
    $pseudo = $div->plaintext;
    $pseudo = str_replace('Pseudo : ','',$pseudo);
    return $pseudo;
}

function getPrenom($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(1);
    $prenom = $div->plaintext;
    $prenom = str_replace('Prénom : ','',$prenom);
    return $prenom;
}

function getNom($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(2);
    $nom = $div->plaintext;
    $nom = str_replace('Nom : ','',$nom);
    return $nom;
}

function getAge($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(3);
    $age = $div->plaintext;
    $age = str_replace('Age : ','',$age);
    return $age;
}

function getVille($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(4);
    $ville = $div->plaintext;
    $ville = str_replace('Ville : ','',$ville);
    return $ville;
}

function getPays($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->children(5);
    $pays = $div->plaintext;
    $pays = str_replace('Pays : ','',$pays);
    return $pays;
}

function getAvatarLink($content, $userId){
    $div = $content->find('div[class=gravatar]')[0]->children(0);
    return $div->src;
}
